'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Room extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Room.belongsTo(models.User, {
        foreignKey: 'hostId',
        onDelete: 'CASCADE'
      });
      Room.belongsTo(models.User, {
        foreignKey: 'targetId',
        onDelete: 'CASCADE'
      });
    }
  }
  Room.init({
    hostId: DataTypes.INTEGER,
    targetId: DataTypes.INTEGER,
    result1: {
      type: DataTypes.STRING,
      allowNull: true
    },
    result2: {
      type: DataTypes.STRING,
      allowNull: true
    },
    result3: {
      type: DataTypes.STRING,
      allowNull: true
    },
    hostChoice1: {
      type: DataTypes.STRING,
      allowNull: true
    },
    hostChoice2: {
      type: DataTypes.STRING,
      allowNull: true
    },
    hostChoice3: {
      type: DataTypes.STRING,
      allowNull: true
    },
    targetChoice1: {
      type: DataTypes.STRING,
      allowNull: true
    },
    targetChoice2: {
      type: DataTypes.STRING,
      allowNull: true
    },
    targetChoice3: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
    sequelize,
    modelName: 'Room',
  });
  return Room;
};