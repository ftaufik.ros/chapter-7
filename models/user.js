'use strict';
const {
  Model
} = require('sequelize');

const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User.hasOne(models.userBiodata, {
        foreignKey: 'userId'
      });
      User.hasMany(models.userGameHistories, {
        foreignKey: 'userId'
      });
    }
  }
  User.init({
    userName: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
        notEmpty: true,
        len: 5,
        isAlphanumeric: true,
        isUnique: (value, next) => {
          User.findAll({
            where: { userName: value }
          })
          .then((user) => {
            if (user.length != 0) {
              next(new Error('username already in use'));
            }
            next();
          });
        }
      }
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    password: {
      type: DataTypes.STRING,
      validate: {
        is: /^[0-9a-zA-Z]{6}$/i
      }
    },
    isAdmin: {
      type: DataTypes.BOOLEAN
    }
  }, 
  {
    sequelize,
    modelName: 'User',
  });

  User.addHook('afterValidate', (user, options) => {
    const salt = bcrypt.genSaltSync();
    user.password = bcrypt.hashSync(user.password, salt);
  });

  User.prototype.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
  };
  
  User.prototype.generateToken = function() {
    const payload = {
      id: this.id,
      userName: this.userName,
      isAdmin: this.isAdmin
    }

    const token = jwt.sign(payload, process.env.JWT_SECRET || 'secret', { expiresIn: '1 day' });
    return token
  };

  return User;
};