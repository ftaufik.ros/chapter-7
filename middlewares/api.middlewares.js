const jwt = require("jsonwebtoken");
const { error } = require("../utils/response/json.utils");

module.exports = {
    verifyJWT: async (req, res, next) => {
        const authHeaders = req.headers['authorization'];
        const token = authHeaders && authHeaders.split(' ')[1];

        jwt.verify(token, process.env.JWT_SECRET || 'secret', (err, user) => {
            if (err) {
                return error(res, 401, 'unauthorized', err);
            }

            req.user = user;
            next();
        });
    },
    emptyChoice: async (req, res, next) => {
        if (req.body.choice === '') return error(res, 400, 'Choice tidak boleh kosong', {
            R: 'FOR ROCK', 
            P: 'FOR PAPER', 
            S: 'FOR SCISSOR'
        });

        next();
    }
};