const { User, userGameHistories } = require("../models");

module.exports = {
    createHistoryForm: async (req, res) => {
        User.findByPk(req.params.id, {
            include: userGameHistories
        })
        .then(users => {
            res.render('user-game/user-history/createHistory', {
                session: req.session,
                users: users
            });
        });
    },
    updateHistoryForm: async (req, res) => {
        userGameHistories.findByPk(req.params.id)
        .then(history => {
            res.render('user-game/user-history/editHistory', {
                session: req.session,
                history: history
            });
        });
    }
};