const { userGameHistories, Room } = require("../../../models");
const { success, error } = require("../../../utils/response/json.utils");

module.exports = {
    getHistory: async (req, res) => {
        try {
            if (req.user.id == req.params.id) {
                let history = await userGameHistories.findAll({
                    where: { userId: req.user.id }
                });
    
                if (!history) {
                    return error(res, 404, 'Belum ada history');
                }
    
                return success(res, 200, 'History anda', { history });
            } else {
                return error(res, 404, 'Periksa kembali userID anda')
            }
        } catch (err) {
            return error(res, 400, err.message, {});
        }
    }
};