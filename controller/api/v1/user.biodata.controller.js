const { userBiodata } = require("../../../models");
const { success, error } = require("../../../utils/response/json.utils");

module.exports = {
    createBiodata: async (req, res) => {
        try {
            let biodata = await userBiodata.create({
                userId: req.params.id,
                fullName: req.body.fullName,
                age: req.body.age,
                country: req.body.country
            });

            return success(res, 201, `Biodata berhasil ditambahkan ke ${req.user.userName}`, biodata);
        } catch (err) {
            return error(res, 400, err.message, {});
        }
    },
    editBiodata : async (req, res) => {
        try {
            let biodata = await userBiodata.findOne({
                where: { userId: req.params.id }
            });
            biodata.fullName = req.body.fullName ? req.body.fullName : biodata.fullName;
            biodata.age = req.body.age ? req.body.age : biodata.age;
            biodata.country = req.body.country ? req.body.country : biodata.country;
            await biodata.save();
            
            return success(res, 201, `Biodata "${req.user.userName}" berhasil diubah`, biodata);
        } catch (err) {
            return error(res, 400, err.message, {});
        }
    }
};