const { User, userBiodata, userGameHistories } = require("../../../models");
const { error, success } = require("../../../utils/response/json.utils");

module.exports = {
    login: async (req, res) => {
        let message = 'Login failed, username/password does not match';

        try {
            let user = await User.findOne({
                where: { userName: req.body.userName }
            });
    
            // Password not valid
            if (!user.validPassword(req.body.password)) {
                return error(res, 400, message, {});
            }

            // If Admin
            if (user.isAdmin) {
                return error(res, 401, 'Unauthorize', {});
            }

            // If Valid
            return success(res, 200, 'Anda berhasil login', {
                token: user.generateToken()
            });
        } catch (err) {
            return error(res, 400, message, {});
        }
    },
    register: async (req, res) => {
        try {
            let user = await User.create({
                userName: req.body.userName,
                email: req.body.email,
                password: req.body.password
            });

            return success(res, 201, 'Register berhasil, silahkan login');
        } catch (err) {
            return error(res, 400, err.message, {})
        }
    },
    verify: async (req, res) => {
        let user = await User.findOne({
            where: { id: req.user.id }
        });

        let biodata = await userBiodata.findOne({
            where: { userId: req.user.id }
        });
        
        let history = await userGameHistories.findAll({
            where: { userId: req.user.id }
        });
        
        return success(res, 200, 'OK', { user, biodata, history })
    }
}