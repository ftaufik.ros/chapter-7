const { User, Room, userGameHistories } = require("../../../models");
const { success, error } = require("../../../utils/response/json.utils");

module.exports = {
    createRoom: async (req, res) => {
        try {
            let target = await User.findOne({
                where: { userName: req.body.targetName }
            });

            let room = await Room.create({
                hostId: req.user.id,
                targetId: target.id
            });

            return success(res, 201, 'Room ID berhasil dibuat, silahkan gunakan untuk bermain', { roomId: room.id });
        } catch (err) {
            let target = await User.findOne({
                where: { userName: req.body.targetName }
            });

            let room = await Room.create({
                hostId: req.user.id,
                targetId: target.id
            });

            return error(res, 400, err.message, {});
        }
    },
    fight: async (req, res) => {

        // Mencari Pemenang utk tampil sebagai message
        const getWinner = (host, target) => {
            if (host === 'S' && target === 'R' || host === 'R' && target === 'P' || host === 'P' && target === 'S'){
                return 'Target Win'
            }
            else if (host === 'S' && target === 'P' || host === 'P' && target === 'R' || host === 'R' && target === 'S') {
                return 'Host Win'
            }
            else {
                return 'Draw'
            }
        };

        try {
            let room = await Room.findOne({
                where: { id: req.params.id }
            });

            // Mencari hasil lalu Insert into history
            function getHistory(r1, r2, r3) {
                if ( 
                    r1 === 'Draw' && r2 === 'Draw' && r3 === 'Draw' || 
                    r1 === 'Host Win' && r2 === 'Draw' && r3 === 'Target Win' || 
                    r1 === 'Host Win' && r2 === 'Target Win' && r3 === 'Draw' ||
                    r1 === 'Target Win' && r2 === 'Host Win' && r3 === 'Draw' || 
                    r1 === 'Target Win' && r2 === 'Draw' && r3 === 'Host Win' || 
                    r1 === 'Draw' && r2 === 'Target Win' && r3 === 'Host Win' || 
                    r1 === 'Draw' && r2 === 'Host Win' && r3 === 'Target Win'
                ) {
                    let host = 'Draw'
                    let target = 'Draw'

                    return [host, target];
                    
                } else if (
                    r1 === 'Host Win' && r2 === 'Host Win' && r3 === 'Host Win' || 
                    r1 === 'Host Win' && r2 === 'Host Win' && r3 === 'Target Win' ||
                    r1 === 'Host Win' && r2 === 'Target Win' && r3 === 'Target Win' ||
                    r1 === 'Target Win' && r2 === 'Host Win' && r3 === 'Host Win' ||
                    r1 === 'Host Win' && r2 === 'Host Win' && r3 === 'Draw' ||
                    r1 === 'Host Win' && r2 === 'Draw' && r3 === 'Host Win' ||
                    r1 === 'Draw' && r2 === 'Host Win' && r3 === 'Host Win' ||
                    r1 === 'Draw' && r2 === 'Draw' && r3 === 'Host Win' ||
                    r1 === 'Draw' && r2 === 'Host Win' && r3 === 'Draw' ||
                    r1 === 'Host Win' && r2 === 'Draw' && r3 === 'Draw'
                ) {
                    let host = 'Win'
                    let target = 'Lose'

                    return [host, target];

                } else {
                    let host = 'Lose'
                    let target = 'Win'

                    return [host, target];
                }
            }
            
            // Cek Host
            if (req.user.id == room.hostId) {
                if (room.result1 && room.result1 !== '' && room.result2 && room.result2 !== '' && room.result3 && room.result3 !== '') {
                    // cari history
                    let historyResult = await userGameHistories.findOne({
                        where: {
                            userId: req.user.id,
                            roomId: room.id 
                        }
                    });

                    // Jika history null Insert into game history
                    if ( !historyResult && room.hostId == req.user.id ) {
                        
                        let histories = getHistory(room.result1, room.result2, room.result3);

                        let historyHost = await userGameHistories.create({
                            userId: room.hostId,
                            roomId: room.id,
                            result: histories[0]
                        });
                    
                        let historyTarget = await userGameHistories.create({
                            userId: room.targetId,
                            roomId: room.id,
                            result: histories[1]
                        });
                    }

                    return success(res, 200, 'Game Selesai, Lihat history untuk melihat seluruh catatan history anda', {
                        game1: {
                            result: room.result1,
                            host: room.hostChoice1, 
                            target: room.targetChoice1
                        },
                        game2: {
                            result: room.result2,
                            host: room.hostChoice2,
                            target: room.targetChoice2
                        },
                        game3: {
                            result: room.result3,
                            host: room.hostChoice3,
                            target: room.targetChoice3
                        }
                    });
                }
                else if ( !room.result1 || room.result1 === '' ) {
                    if( !room.hostChoice1 ){
                        room.hostChoice1 = req.body.choice;
                        await room.save();
            
                        if( !room.targetChoice1 ) {
                            return success(res, 200, 'Game 1: Menunggu Lawan...', {
                                game1: {
                                    host: room.hostChoice1, 
                                    target: room.targetChoice1
                                }
                            });
                        }

                        let hasil1 = getWinner(room.hostChoice1, room.targetChoice1);
                        room.result1 = hasil1;
                        await room.save();
            
                        return success(res, 201, `Game 1: ${room.result1}`, {
                            game1: {
                                host: room.hostChoice1, 
                                target: room.targetChoice1
                            }
                        });
                    }
                } else if ( !room.result2 || room.result2 === '' ) {
                    if( !room.hostChoice2 ){
                        room.hostChoice2 = req.body.choice;
                        await room.save();
            
                        if(room.targetChoice2 == null) {
                            return success(res, 200, 'Game 2: Menunggu Lawan...', {
                                game1: {
                                    host: room.hostChoice1, 
                                    target: room.targetChoice1
                                },
                                game2: {
                                    host: room.hostChoice2,
                                    target: room.targetChoice2
                                }
                            });
                        }

                        let hasil2 = getWinner(room.hostChoice2, room.targetChoice2);
                        room.result2 = hasil2;
                        await room.save();
            
                        return success(res, 201, `Game 2: ${room.result2}`, {
                            game1: {
                                host: room.hostChoice1, 
                                target: room.targetChoice1
                            },
                            game2: {
                                host: room.hostChoice2,
                                target: room.targetChoice2
                            }
                        });
                    }
                } else if ( !room.result3 || room.result3 === '' ) {
                    if( !room.hostChoice3 || room.hostChoice3 === '' ){
                        room.hostChoice3 = req.body.choice;
                        await room.save();
            
                        if( !room.targetChoice3 ) {
                            return success(res, 200, 'Game 3: Menunggu Lawan...', {
                                game1: {
                                    host: room.hostChoice1, 
                                    target: room.targetChoice1
                                },
                                game2: {
                                    host: room.hostChoice2,
                                    target: room.targetChoice2
                                },
                                game3: {
                                    host: room.hostChoice3,
                                    target: room.targetChoice3
                                }
                            });
                        }

                        let hasil3 = getWinner(room.hostChoice3, room.targetChoice3);
                        room.result3 = hasil3;
                        await room.save();
            
                        return success(res, 201, `Game 3: ${room.result3}`, {
                            game1: {
                                host: room.hostChoice1, 
                                target: room.targetChoice1
                            },
                            game2: {
                                host: room.hostChoice2,
                                target: room.targetChoice2
                            },
                            game3: {
                                host: room.hostChoice3,
                                target: room.targetChoice3
                            }
                        });
                    }
                } else { 
                    return error(res, 400, 'Unknown')
                }
            // Cek Target
            } else if (req.user.id == room.targetId) {
                if (room.result1 && room.result1 !== '' && room.result2 && room.result2 !== '' && room.result3 && room.result3 !== '') {
                    // cari history
                    let historyResult = await userGameHistories.findOne({
                        where: {
                            userId: req.user.id,
                            roomId: room.id 
                        }
                    });

                    // Jika history null Insert into game history
                    if ( !historyResult && room.targetId == req.user.id ) {
                        
                        let histories = getHistory(room.result1, room.result2, room.result3);

                        let historyHost = await userGameHistories.create({
                            userId: room.hostId,
                            roomId: room.id,
                            result: histories[0]
                        });
                    
                        let historyTarget = await userGameHistories.create({
                            userId: room.targetId,
                            roomId: room.id,
                            result: histories[1]
                        });
                    }

                    return success(res, 200, 'Game Selesai, Lihat history untuk melihat seluruh catatan history anda', {
                        game1: {
                            result: room.result1,
                            host: room.hostChoice1, 
                            target: room.targetChoice1
                        },
                        game2: {
                            result: room.result2,
                            host: room.hostChoice2,
                            target: room.targetChoice2
                        },
                        game3: {
                            result: room.result3,
                            host: room.hostChoice3,
                            target: room.targetChoice3
                        }
                    });
                }
                else if ( !room.result1 || room.result1 === '' ) {
                    if ( !room.targetChoice1 ){
                        room.targetChoice1 = req.body.choice;
                        await room.save();
            
                        if( !room.hostChoice1 ) {
                            return success(res, 200, 'Game 1: Menunggu Lawan...', {
                                game1: {
                                    host: room.hostChoice1, 
                                    target: room.targetChoice1
                                }
                            });
                        }

                        let hasil1 = getWinner(room.hostChoice1, room.targetChoice1);
                        room.result1 = hasil1;
                        await room.save();
            
                        return success(res, 201, `Game 1: ${room.result1}`, {
                            game1: {
                                host: room.hostChoice1, 
                                target: room.targetChoice1
                            }
                        });
                    }
                } else if ( !room.result2 || room.result2 === '' ) {
                    if ( !room.targetChoice2 ){
                        room.targetChoice2 = req.body.choice;
                        await room.save();
            
                        if( !room.hostChoice2 ) {
                            return success(res, 200, 'Game 2: Menunggu Lawan...', {
                                game1: {
                                    host: room.hostChoice1, 
                                    target: room.targetChoice1
                                },
                                game2: {
                                    host: room.hostChoice2,
                                    target: room.targetChoice2
                                }
                            });
                        }

                        let hasil2 = getWinner(room.hostChoice2, room.targetChoice2);
                        room.result2 = hasil2;
                        await room.save();
            
                        return success(res, 201, `Game 2: ${room.result2}`, {
                            game1: {
                                host: room.hostChoice1, 
                                target: room.targetChoice1
                            },
                            game2: {
                                host: room.hostChoice2,
                                target: room.targetChoice2
                            }
                        });
                    }
                } else if ( !room.result3 || room.result3 === '' ) {
                    if ( !room.targetChoice3 || room.targetChoice3 === '' ){
                        room.targetChoice3 = req.body.choice;
                        await room.save();
            
                        if( !room.hostChoice3 ) {
                            return success(res, 200, 'Game 3: Menunggu Lawan...', {
                                game1: {
                                    host: room.hostChoice1, 
                                    target: room.targetChoice1
                                },
                                game2: {
                                    host: room.hostChoice2,
                                    target: room.targetChoice2
                                },
                                game3: {
                                    host: room.hostChoice3,
                                    target: room.targetChoice3
                                }
                            });
                        }

                        let hasil3 = getWinner(room.hostChoice3, room.targetChoice3);
                        room.result3 = hasil3;
                        await room.save();
            
                        return success(res, 201, `Game 2: ${room.result3}`, {
                            game1: {
                                host: room.hostChoice1, 
                                target: room.targetChoice1
                            },
                            game2: {
                                host: room.hostChoice2,
                                target: room.targetChoice2
                            },
                            game3: {
                                host: room.hostChoice3,
                                target: room.targetChoice3
                            }
                        });
                    }
                } else { 
                    return error(res, 400, 'Unknown')
                }
            // Jika bukan Host atau Target
            } else {
                return error(res, 400, 'Periksa Kembali ID Room Anda');
            };
        } catch (err) {
            return error(res, 400, err.message, {});
        }
    }
};