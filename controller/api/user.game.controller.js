const { User, userBiodata, userGameHistories } = require("../../models");

module.exports = {
    create: async (req, res) => {
        try {
            let user = await User.create({
                userName: req.body.userName,
                email: req.body.email,
                password: req.body.password,
            });
            let biodata = await userBiodata.create({
                userId: user.id,
                fullName: req.body.fullName,
                age: req.body.age,
                country: req.body.country,
            });
            res.redirect('/home');
        } catch (error) {
            res.render('user-game/create', {
                error: error
            });
        }
    },
    edit: async (req, res) => {
        try {
            let users = await User.findByPk(req.body.id);
            users.userName = req.body.userName ? req.body.userName : users.userName;
            users.email = req.body.email ? req.body.email : users.email;
            users.password = req.body.password ? req.body.password : users.password;
            await users.save();

            res.redirect('/home');
        } catch (error) {
            res.render('user-game/edit', {
                error: error
            });
        }
    },
    destroy: async (req, res) => {
        try {
            const count = await User.destroy({
                where: { id: req.params.id }
            });

            if (count > 0) {
                return res.redirect('/home');
            } else {
                return res.redirect('/home');
            }
        } catch (err) {
            return res.redirect('/home');
        };
    }
};
