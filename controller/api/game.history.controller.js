const { User, userGameHistories } = require("../../models");

module.exports = {
    createHistory: async (req, res) => {
        try {
            let history = await userGameHistories.create({
                userId: req.body.id,
                roomId: req.body.roomId,
                result: req.body.result
            });
            
            res.redirect(`/user/show/${req.body.id}`);
        } catch (error) {
            res.render('user-game/user-history/createHistory', {
                error: error
            });
        }
    },
    editHistory: async (req, res) => {
        try {
            let history = await userGameHistories.findByPk(req.body.id);
            history.roomId = req.body.roomId ? req.body.roomId : history.roomId;
            history.result = req.body.result ? req.body.result : history.result;
            await history.save();

            res.redirect(`/user/show/${req.body.userId}`);
        } catch (error) {
            res.render('user-game/user-history/editHistory', {
                error: error
            });
        }
    },
    destroyHistory: async (req, res) => {
        try {
            const count = await userGameHistories.destroy({
                where: { id: req.params.id }
            });

            if (count > 0) {
                return res.redirect(`/home`);
            } else {
                return res.redirect(`/home`);
            }
        } catch (err) {
            return res.render('/home');
        };
    }
};