const { User } = require("../../models");

module.exports = {
    register: async (req, res) => {
        try {
            let user = await User.create({
                userName: req.body.userName,
                email: req.body.email,
                password: req.body.password,
            });

            res.redirect("/login");
        } catch (error) {
            res.render("register", {
                message: {
                    type: 'error',
                    message: error
                }
            })
        }
    },
    login: async (req, res) => {
        let message = {
            type: 'error',
            message: 'Login failed, username/password does not match'
        };

        try {
            let user = await User.findOne({
                where: { userName: req.body.userName }
            });
            
            // Password not valid
            if (!user.validPassword(req.body.password)) {
                return res.render('login', {
                    message: message
                })
            }

            // If valid, store to session
            req.session.userId = user.id;
            req.session.userName = user.userName;
            req.session.isAdmin = user.isAdmin == true;
            
            res.redirect('/home');
        } catch (error) {
            res.render('login', {
                error: error
            });
        }
    },
    logout: async (req, res) => {
        if (req.session.userId) {
            req.session.destroy();
            res.redirect('/');
        }
    }
};