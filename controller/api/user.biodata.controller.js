const { User, userBiodata } = require("../../models");

module.exports = {
    editBiodata : async (req, res) => {
        try {
            let biodataUser = await userBiodata.findByPk(req.body.id);
            biodataUser.fullName = req.body.fullName ? req.body.fullName : biodataUser.fullName;
            biodataUser.age = req.body.age ? req.body.age : biodataUser.age;
            biodataUser.country = req.body.country ? req.body.country : biodataUser.country;
            await biodataUser.save();

            res.redirect(`/user/show/${req.body.userId}`);
        } catch (error) {
            res.render('user-game/user-biodata/editBiodata', {
                error: error
            });
        }
    }
};