// const { superUsers } = require('../../data/superUser');
const { User, userBiodata } = require("../models");

module.exports= {
    home: async (req, res) => {
        let whereQuery = req.session.isAdmin ? { isAdmin: null } : { id: req.session.userId };

        await User.findAll({
            where: whereQuery,
            order: [
                ['updatedAt', 'desc'],
                // ['createdAt', 'desc']
            ],
            include: userBiodata
        })
        .then(users => {
            res.render('user-game/home', {
                session: req.session,
                users: users
            });
        });
    },
    loginPage: async (req, res) => {
        res.render('login', {
            session: req.session
        });
    },
    registerPage: (req, res) => {
        res.render('register', {
            session: req.session
        });
    },
    index: (req, res) => {
        res.render('index', {
            session: req.session
        });
    }
};