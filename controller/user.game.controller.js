const { User, userBiodata, userGameHistories } = require("../models");

module.exports = {
    createForm: async (req, res) => {
        User.findByPk(req.params.id, {
            include: userBiodata
        })
        .then(users => {
            res.render('user-game/create', {
                session: req.session,
                users: users
            });
        });
    },
    updateForm: async (req, res) => {
        User.findByPk(req.params.id, {
            include: userBiodata
        })
        .then(users => {
            res.render('user-game/edit', {
                session: req.session,
                users: users
            });
        });
    },
    show: async function (req, res) {
        User.findByPk(req.params.id, {
            include: [{
                model : userBiodata
            },
            {
                model : userGameHistories
            }]
        })
        .then(users => {
            res.render('user-game/show', {
                session: req.session,
                users: users
            });
        });
    }
};
