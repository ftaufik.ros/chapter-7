const { User, userBiodata } = require("../models");

module.exports = {
    editBiodataForm: async (req, res) => {
        User.findByPk(req.params.id, {
            include: userBiodata
        })
        .then(bio => {
            res.render('user-game/user-biodata/editBiodata', {
                session: req.session,
                bio: bio
            });
        });
    }
};