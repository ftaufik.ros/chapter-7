# Challange Chapter 7

### About This Project :

- Continue previous projects (chapter-6)
- Implementing Design Pattern : MVC and MCR
- Implementing Login with database
- Implementing bcrypt
- Implementing Sequelize Hooks
- Add user role 'isAdmin'
- Implementing data ownership
- Storing session
- Build new feature REST API for game Rock, Paper, Scrissor
