const userGame = require("../controller/user.game.controller");
const webMiddleware = require("../middlewares/web.middlewares");

const router = require("express").Router();

router.get("/create", webMiddleware.verifyWebSession, userGame.createForm);
router.get("/edit/:id", webMiddleware.verifyWebSession, userGame.updateForm);
router.get("/show/:id", webMiddleware.verifyWebSession, userGame.show);

module.exports = router;