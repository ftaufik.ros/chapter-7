const homeController = require("../controller/home.controller");
const webMiddleware = require("../middlewares/web.middlewares");
const router = require("express").Router();

router.get("/", webMiddleware.redirectIfSigned, homeController.index);
router.get("/login", webMiddleware.redirectIfSigned, homeController.loginPage);
router.get("/register", webMiddleware.redirectIfSigned, homeController.registerPage);
router.get("/home", webMiddleware.verifyWebSession,homeController.home);

module.exports = router;