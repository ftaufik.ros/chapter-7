const userGameHistory = require("../controller/game.history.controller");
const webMiddleware = require("../middlewares/web.middlewares");

const router = require("express").Router();

router.get("/create/:id", webMiddleware.verifyWebSession, userGameHistory.createHistoryForm);
router.get("/edit/:id", webMiddleware.verifyWebSession, userGameHistory.updateHistoryForm);

module.exports = router;