const router = require("express").Router();

const homeRouter = require("./home.router");
const userGameRouter = require("./user.game.router");
const userBiodataRouter = require("./user.biodata.router");
const userGamehistoryRouter = require("./game.history.router");

const apiUserRouter = require("./api/user.router");
const apiUserGameRouter = require("./api/user.game.router");
const apiUserBiodataRouter = require("./api/user.biodata.router");
const apiUserGameHistoryRouter = require("./api/game.history.router");

const apiV1UserRouter = require("./api/v1/user.v1.router");
const apiV1UserBiodataRouter = require("./api/v1/user.biodata.v1.router");
const apiV1UserGameHistory = require("./api/v1/game.history.v1.router");
const apiV1Game = require("./api/v1/game.v1.router");


// MVC
router.use("/", homeRouter);
router.use("/user", userGameRouter);
router.use("/user/biodata", userBiodataRouter);
router.use("/user/game-history", userGamehistoryRouter);

// MCR
router.use("/api", apiUserRouter);
router.use("/api/user", apiUserGameRouter);
router.use("/api/user/biodata", apiUserBiodataRouter);
router.use("/api/user/game-history", apiUserGameHistoryRouter);

// API
router.group("/api/v1", route => {
    route.use("/user", apiV1UserRouter);
    route.use("/user/biodata", apiV1UserBiodataRouter);
    route.use("/user/history", apiV1UserGameHistory);
    route.use("/start", apiV1Game);
});

module.exports = router;