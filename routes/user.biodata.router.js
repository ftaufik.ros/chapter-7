const userBiodata = require("../controller/user.biodata.controller");
const webMiddleware = require("../middlewares/web.middlewares");

const router = require("express").Router();

router.get("/edit/:id", webMiddleware.verifyWebSession, userBiodata.editBiodataForm);

module.exports = router;