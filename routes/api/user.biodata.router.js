const router = require("express").Router();
const bodyParser = require("body-parser");
const userBiodataController = require("../../controller/api/user.biodata.controller");

const jsonParser = bodyParser.json();
const urlEncoded = bodyParser.urlencoded({ extended: false });

router.post("/edit", urlEncoded, userBiodataController.editBiodata);

module.exports = router;