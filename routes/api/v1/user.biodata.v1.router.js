const router = require("express").Router();
const bodyParser = require("body-parser");
require("express-group-routes");

const jsonParser = bodyParser.json();
const userBiodataControllerV1 = require("../../../controller/api/v1/user.biodata.controller");
const { verifyJWT } = require("../../../middlewares/api.middlewares");


router.post("/create/:id", jsonParser, verifyJWT, userBiodataControllerV1.createBiodata);
router.put("/edit/:id", jsonParser, verifyJWT, userBiodataControllerV1.editBiodata);

module.exports = router;