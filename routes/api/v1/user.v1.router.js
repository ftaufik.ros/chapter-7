const router = require("express").Router();
const bodyParser = require("body-parser");
require("express-group-routes");

const jsonParser = bodyParser.json();
const userV1Controller = require("../../../controller/api/v1/user.controller");
const { verifyJWT } = require("../../../middlewares/api.middlewares");

router.post("/login", jsonParser, userV1Controller.login);
router.post("/register", jsonParser, userV1Controller.register);
router.get("/verify", jsonParser, verifyJWT, userV1Controller.verify);

module.exports = router;