const router = require("express").Router();
const bodyParser = require("body-parser");
require("express-group-routes");

const jsonParser = bodyParser.json();
const roomControllerV1 = require("../../../controller/api/v1/game.controller")
const { verifyJWT, emptyChoice } = require("../../../middlewares/api.middlewares");


router.post("/create-room", jsonParser, verifyJWT, roomControllerV1.createRoom);
router.post("/fight/:id", jsonParser, verifyJWT, emptyChoice, roomControllerV1.fight);

module.exports = router;