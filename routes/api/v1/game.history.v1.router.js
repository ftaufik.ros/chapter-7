const router = require("express").Router();
const bodyParser = require("body-parser");
const userGameHistoryControllerV1 = require("../../../controller/api/v1/game.history.controller");
const { verifyJWT } = require("../../../middlewares/api.middlewares");

const jsonParser = bodyParser.json();
const urlEncoded = bodyParser.urlencoded({ extended: false });

router.get("/show/:id", verifyJWT, userGameHistoryControllerV1.getHistory);

module.exports = router;