const router = require("express").Router();
const bodyParser = require("body-parser");
const userGameController = require("../../controller/api/user.game.controller");

const jsonParser = bodyParser.json();
const urlEncoded = bodyParser.urlencoded({ extended: false });

router.post("/create", urlEncoded, userGameController.create);
router.post("/edit", urlEncoded, userGameController.edit);
router.get("/delete/:id", userGameController.destroy);

module.exports = router;