const router = require("express").Router();
const bodyParser = require("body-parser");
const userGameHistoryController = require("../../controller/api/game.history.controller");

const jsonParser = bodyParser.json();
const urlEncoded = bodyParser.urlencoded({ extended: false });

router.post("/edit", urlEncoded, userGameHistoryController.editHistory);
router.post("/create", urlEncoded, userGameHistoryController.createHistory);
router.get("/delete/:id", userGameHistoryController.destroyHistory);

module.exports = router;