const router = require("express").Router();
const bodyParser = require("body-parser");
require("express-group-routes");

const userController = require("../../controller/api/user.controller");

const jsonParser = bodyParser.json();
const urlEncoded = bodyParser.urlencoded({ extended: false });

router.post("/login", urlEncoded, userController.login);
router.get("/logout", userController.logout);
// router.post("/register", urlEncoded, userController.register);

module.exports = router;