function checkValue() {
    let inputt = document.querySelectorAll('input');
    if (inputt[0].value.length == 0 || inputt[1].value.length == 0 || inputt[2].value.length == 0 || inputt[3].value.length == 0 || inputt[4].value.length == 0 || inputt[5].value.length == 0) {
        return document.getElementById('saveBtn').disabled = true;
    } else {
        return document.getElementById('saveBtn').disabled = false;
    };
};

function checkEditUser() {
    let inputt = document.querySelectorAll('input');
    if (inputt[0].value.length == 0 || inputt[1].value.length == 0 || inputt[2].value.length == 0 || inputt[3].value.length == 0) {
        return document.getElementById('saveBtn').disabled = true;
    } else {
        return document.getElementById('saveBtn').disabled = false;
    };
};

function checkCreateHistory() {
    let inputt = document.querySelectorAll('input');
    if (inputt[0].value.length == 0 || inputt[1].value.length == 0 || inputt[2].value.length == 0) {
        return document.getElementById('saveBtn').disabled = true;
    } else {
        return document.getElementById('saveBtn').disabled = false;
    };
};

function checkEditHistory() {
    let inputt = document.querySelectorAll('input');
    if (inputt[0].value.length == 0 || inputt[1].value.length == 0 || inputt[2].value.length == 0 || inputt[3].value.length == 0) {
        return document.getElementById('saveBtn').disabled = true;
    } else {
        return document.getElementById('saveBtn').disabled = false;
    };
};

function checkEditBio() {
    let inputt = document.querySelectorAll('input');
    if (inputt[0].value.length == 0 || inputt[1].value.length == 0 || inputt[2].value.length == 0 || inputt[3].value.length == 0 || inputt[4].value.length == 0) {
        return document.getElementById('saveBtn').disabled = true;
    } else {
        return document.getElementById('saveBtn').disabled = false;
    };
};

function checkLogin() {
    let inputt = document.querySelectorAll('input');
    if (inputt[0].value.length == 0 || inputt[1].value.length == 0) {
        return document.getElementById('saveBtn').disabled = true;
    } else {
        return document.getElementById('saveBtn').disabled = false;
    };
};