'use strict';
const DataTypes = require('sequelize/lib/data-types');

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.addColumn('Rooms', 'result1', { type: DataTypes.STRING });
    await queryInterface.addColumn('Rooms', 'result2', { type: DataTypes.STRING });
    await queryInterface.addColumn('Rooms', 'result3', { type: DataTypes.STRING });

    await queryInterface.addColumn('Rooms', 'hostChoice1', { type: DataTypes.STRING });
    await queryInterface.addColumn('Rooms', 'hostChoice2', { type: DataTypes.STRING });
    await queryInterface.addColumn('Rooms', 'hostChoice3', { type: DataTypes.STRING });

    await queryInterface.addColumn('Rooms', 'targetChoice1', { type: DataTypes.STRING });
    await queryInterface.addColumn('Rooms', 'targetChoice2', { type: DataTypes.STRING });
    await queryInterface.addColumn('Rooms', 'targetChoice3', { type: DataTypes.STRING });
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.removeColumn('Rooms', 'result1');
    await queryInterface.removeColumn('Rooms', 'result2');
    await queryInterface.removeColumn('Rooms', 'result3');

    await queryInterface.removeColumn('Rooms', 'hostChoice1');
    await queryInterface.removeColumn('Rooms', 'hostChoice2');
    await queryInterface.removeColumn('Rooms', 'hostChoice3');

    await queryInterface.removeColumn('Rooms', 'targetChoice1');
    await queryInterface.removeColumn('Rooms', 'targetChoice2');
    await queryInterface.removeColumn('Rooms', 'targetChoice3');
  }
};
