'use strict';
const DataTypes = require('sequelize/lib/data-types');

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.addColumn('userGameHistories', 'roomId', { type: DataTypes.INTEGER });
    await queryInterface.addColumn('userGameHistories', 'result', { type: DataTypes.STRING });
    await queryInterface.removeColumn('userGameHistories', 'gameName');
    await queryInterface.removeColumn('userGameHistories', 'playTime');
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.removeColumn('userGameHistories', 'roomId');
    await queryInterface.removeColumn('userGameHistories', 'result');
    await queryInterface.addColumn('userGameHistories', 'gameName', { type: DataTypes.STRING });
    await queryInterface.addColumn('userGameHistories', 'playTime', { type: DataTypes.STRING });
  }
};
